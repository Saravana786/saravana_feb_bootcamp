package week5.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import week5.pom.BaseClass;

public class OpportunityPage extends BaseClass {
	
	public OpportunityPage(RemoteWebDriver driver)
	{
		this.driver=driver;
	}

	public OpportunityPage selectOpportunity()
	{
		WebElement element=driver.findElement(By.xpath(prop.getProperty("OppoPage.selectOppo.xpath")));
		//wait.until(ExpectedConditions.elementToBeClickable(element));
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].click();", element);
		return this;
	}
	
	public OpportunityPage newOpportunity() throws InterruptedException
	{
		Thread.sleep(2000);
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@title='New']"))));
		driver.findElement(By.xpath(prop.getProperty("OppoPage.newOppo.xpath"))).click();
		return this;
	}
	
	public OpportunityPage inputname(String name)
	{
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='Name']"))));
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='CloseDate']"))));
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputname.xpath"))).sendKeys(name);
		return this;
	}
	
	public OpportunityPage inputCloseDate(String todayDate)
	{
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='Name']"))));
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='CloseDate']"))));
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputdate.xpath"))).sendKeys(todayDate);
		return this;
	}
	
	public OpportunityPage selectNewOpportunity() throws InterruptedException
	{
		Thread.sleep(10000);
		WebElement element1=driver.findElement(By.xpath(prop.getProperty("OppoPage.selectnew.xpath")));
		//wait.until(ExpectedConditions.elementToBeClickable(element1));
		JavascriptExecutor je1=(JavascriptExecutor)driver;
		je1.executeScript("arguments[0].click();", element1);
		return this;
	}
	
	public OpportunityPage needAnalysisselct() throws InterruptedException
	{
		Thread.sleep(10000);
		driver.findElement(By.xpath(prop.getProperty("OppoPage.needanaysis.xpath"))).click();
		return this;
	}
	
	public OpportunityPage SaveOpportunity()
	{
		driver.findElement(By.xpath(prop.getProperty("OppoPage.savenewOppo.xpath"))).click();
		return this;
	}
	
	public OpportunityPage verifyOpportunityPage() throws InterruptedException
	{
		//Validating results
		Thread.sleep(2000);
		String result=driver.findElement(By.xpath(prop.getProperty("OppoPage.verifynewOppotext.xpath"))).getText();
		if (result.contains("created") && result.contains(expected))
		{
			System.out.println("Create Opporunity Pass");
		}
		else
		{
			System.out.println("Create Opporunity Fail");
		}
		return this;
	}
	
	public OpportunityPage searchlist()
	{
		driver.findElement(By.xpath(prop.getProperty("OppoPage.searchlist.xpath"))).sendKeys(expected);
		return this;
		
	}
	
	public OpportunityPage refreshbutton()
	{
		driver.findElement(By.xpath(prop.getProperty("OppoPage.refreshbutton.xpath"))).click();
		return this;
	}
	
	public OpportunityPage selectOpportunityowner()
	{
		WebElement element1=driver.findElement(By.xpath(prop.getProperty("OppoPage.selectOpportunityOwner.xpath")));
		JavascriptExecutor je1=(JavascriptExecutor)driver;
		je1.executeScript("arguments[0].click();", element1);
		return this;
	}
	
	public OpportunityPage showactions() throws InterruptedException
	{
		Thread.sleep(4000);
		WebElement element2=driver.findElement(By.xpath(prop.getProperty("OppoPage.showactions.xpath")));
		JavascriptExecutor je2=(JavascriptExecutor)driver;
		je2.executeScript("arguments[0].click();", element2);
		return this;
	}
	
	public OpportunityPage selectEdit() throws InterruptedException
	{
		Thread.sleep(4000);
		driver.findElement(By.xpath(prop.getProperty("OppoPage.selectEdit.xpath"))).click();
		return this;
	}
	
	public OpportunityPage inputdate() throws InterruptedException
	{
		Thread.sleep(2000); 
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputclosedate.xpath"))).clear();
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputclosedate.xpath"))).sendKeys(tommorowDate);
		return this;
	}
	
	public OpportunityPage selectstage()
	{
		WebElement element3=driver.findElement(By.xpath(prop.getProperty("OppoPage.selectstage.xpath")));
		//button[@data-position-id='lgcp-1000007']//span[1]
		JavascriptExecutor je3=(JavascriptExecutor)driver;
		je3.executeScript("arguments[0].click();", element3);
		return this;
	}
	
	public OpportunityPage selecteditstage() throws InterruptedException
	{
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[@title='"+stage+"']")).click();
		Thread.sleep(1000);
		return this;
	}
	public OpportunityPage clickvalue()
	{
		WebElement element4=driver.findElement(By.xpath(prop.getProperty("OppoPage.selectdrop.xpath")));
		JavascriptExecutor je4=(JavascriptExecutor)driver;
		je4.executeScript("arguments[0].scrollIntoView(true);",element4);
		driver.findElement(By.xpath(prop.getProperty("OppoPage.selectdrop.xpath"))).click();
	    return this;
	}
	
	public OpportunityPage inputvalue() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputvalue.xpath"))).click();
		Thread.sleep(2000);
		return this;
	}
	
	public OpportunityPage editinputvalue() throws InterruptedException
	{
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputenter.xpath"))).clear();
		driver.findElement(By.xpath(prop.getProperty("OppoPage.inputenter.xpath"))).sendKeys(salesforce);
		Thread.sleep(1000);
		return this;
	}
	
	public OpportunityPage toastvalidation()
	{
		WebElement element5=driver.findElement(By.xpath("//span[text()='"+stage+"']"));
		
		//System.out.println(element5.getText());
		
		if (stage.equals(element5.getText()))
		{
			System.out.println("Edit Opporunity Pass");
		}
		else
		{
			System.out.println("Edit Opporunity Fail");
		}
		return this;
	}
	
	public OpportunityPage selectDelete() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("OppoPage.selectDelete.xpath"))).click();
		Thread.sleep(1000);
		return this;
	}
	
	public OpportunityPage clickDelete() throws InterruptedException
	{
		driver.findElement(By.xpath(prop.getProperty("OppoPage.clickDelete.xpath"))).click();
		Thread.sleep(2000);
		return this;
	}
	
	public OpportunityPage deletetoastvalidation()
	{
		String result=driver.findElement(By.xpath(prop.getProperty("OppoPage.deletetoast.xpath"))).getText();
		System.out.println(result);
		
		//System.out.println(expected);
		if (result.contains("deleted") && result.contains(name))
		{
			System.out.println("Delete Opporutniy Pass");
		}
		else
		{
			System.out.println("Delete Opporutniy Fail");
		}
		return this;
	}
}
