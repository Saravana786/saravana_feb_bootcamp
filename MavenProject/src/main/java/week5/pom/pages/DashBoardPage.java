package week5.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import week5.pom.BaseClass;

public class DashBoardPage extends BaseClass {
	
	public DashBoardPage(RemoteWebDriver driver)
	{
		this.driver=driver;
	}
	
	public DashBoardPage clickNewDashboard() throws InterruptedException
	{
		Thread.sleep(5000);
		System.out.println("ok");
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.newdashboard.xpath"))).click();
		return this;
	}
	
	public DashBoardPage frameSwitch() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.switchTo().frame(driver.findElement(By.xpath(prop.getProperty("DashboardPage.frame.xpath"))));
		return this;
		
	}

	
	public DashBoardPage frameback()
	{
		driver.switchTo().defaultContent();
		return this;
		
	}
	public DashBoardPage DashBoardinput(String dashboardText) throws InterruptedException
	{

		driver.findElement(By.id(prop.getProperty("DashboardPage.inputname.id"))).sendKeys(dashboardText);
		Thread.sleep(1000);
		driver.findElement(By.id(prop.getProperty("DashboardPage.submitbutton.id"))).click();
		return this;
	}
	
	public DashBoardPage savechanges()
	{
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Save']"))));
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.savebutton.xpath"))).click();
		return this;
	}
		
	public DashBoardPage verifyText() throws InterruptedException
	{
		Thread.sleep(5000);
		String result=driver.findElement(By.xpath(prop.getProperty("DashboardPage.verifytext.xpath"))).getText();
		
		if (result.contains("saved") && result.contains("Dashboard"))
		{
			System.out.println("Create Dashboard Pass");
		}
		else
		{
			System.out.println("Create Dashboard Fail");
		}
		
		return this;
	}
	
	public DashBoardPage selectEditserach(String dashboardText1 )
	{
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.editserach.xpath"))).sendKeys(dashboardText1);
		return this;
	}
	
	public DashBoardPage clickDropdownDashboard() throws InterruptedException
	{
		Thread.sleep(5000);
		System.out.println("hi");
		WebElement element=driver.findElement(By.xpath(prop.getProperty("DashboardPage.clickdropdown.xpath")));
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].scrollIntoView(true);",element);
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.clickdropdown.xpath"))).click();
		return this;
	}
	
	public DashBoardPage editPage() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.editoption.xpath"))).click();
		return this;
	}
	
	public DashBoardPage deletePage() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.deleteoption.xpath"))).click();
		return this;
	}
	
	public DashBoardPage clickEditPage()
	{
		 //wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']"))));
	        driver.findElement(By.xpath(prop.getProperty("DashboardPage.selectedit.xpath"))).click();
		return this;
	}
	
	public DashBoardPage clickDeletePage()
	{
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.selectDelete.xpath"))).click();
		return this;
	}
	
	public DashBoardPage sendInput()
	{
		  driver.findElement(By.id(prop.getProperty("DashboardPage.sendinput.id"))).sendKeys(description);
		return this;
	}
	
	public DashBoardPage submitButton()
	{
		driver.findElement(By.id(prop.getProperty("DashboardPage.submitbutton.id"))).click();
		return this;
	}
	
	public DashBoardPage clickDoneButton()
	{
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Done']"))));
     	driver.findElement(By.xpath(prop.getProperty("DashboardPage.Donebutton.xpath"))).click();
		return this;
	}
	
	public DashBoardPage clickSavebutton()
	{
		driver.findElement(By.xpath(prop.getProperty("DashboardPage.Saveeditbutton.xpath"))).click();
		return this;
	}
	
	public DashBoardPage verifyEditPage() throws InterruptedException
	{
String result=driver.findElement(By.xpath(prop.getProperty("DashboardPage.edittoasttext.xpath"))).getText();
		
     	Thread.sleep(2000);
		//String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		if (result.contains(description) )
		{
			System.out.println("Edit Dashboard Pass");
		}
		else
		{
			System.out.println("Edit Dashboard Fail");
		}
		
		return this;
	}
	
	public DashBoardPage verifyDeleteToast() throws InterruptedException
	{
		Thread.sleep(2000);
	 	//String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
		String result=driver.findElement(By.xpath(prop.getProperty("DashboardPage.deltetoasttext.xpath"))).getText();
	 	
	 	if (result.contains("Dashboard") && result.contains("deleted"))
		{
			System.out.println("Delete Dashboard Pass");
		}
		else
		{
			System.out.println("Delete Dashboard Fail");
		}
	 	
	 	return this;
	}
	
	
}
