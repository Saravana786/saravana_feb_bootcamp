package week5.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import week5.pom.BaseClass;

public class HomePage extends BaseClass  {
	
	public HomePage(RemoteWebDriver driver)
	{
		this.driver=driver;
				
	}

	
	public HomePage clickTogglebutton() throws InterruptedException
	{
		Thread.sleep(10000);
		System.out.println("inside toggle");
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath(prop.getProperty("HomePage.toggle.xpath"))).click();
		return this;
	}
	
	public HomePage clickViewAll()
	{
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath(prop.getProperty("HomePage.viewall.xpath"))).click();
		return this;
	}
	
	public HomePage search(String input)
	{
		WebElement searchwindow = driver.findElement(By.xpath(prop.getProperty("HomePage.search.xpath")));
		searchwindow.sendKeys(input);
		return this;
	}
		
	public DashBoardPage clickDashboardSelection()
	{
		driver.findElement(By.xpath(prop.getProperty("HomePage.dashboard.xpath"))).click();
		return new DashBoardPage(driver);
	}
	
	public OpportunityPage clickSalesMenu()
	{
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[text()='Sales']"))));
		driver.findElement(By.xpath(prop.getProperty("HomePage.salesmenu.xpath"))).click();
		return new OpportunityPage(driver);
	}
	
	public ContactPage clickbutton() throws InterruptedException
	{

		Thread.sleep(10000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon")));
		
		By button = By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon");
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(button)));
		driver.findElement(button).click();
		Thread.sleep(5000);
		return new ContactPage(driver);
	}

}
