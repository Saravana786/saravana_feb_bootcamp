package week5.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import week5.pom.BaseClass;

public class LoginPage extends BaseClass {
	
	public LoginPage(RemoteWebDriver driver)
	{
		this.driver=driver;
	}

	public LoginPage enterUserName(String username)
	{
		driver.findElement(By.id(prop.getProperty("LoginPage.Username.Id"))).sendKeys(username);
		return this;
	}
	
	public LoginPage enterPassword(String password)
	{
		driver.findElement(By.id(prop.getProperty("LoginPage.Password.Id"))).sendKeys(password);
		return this;
	}
	
	public HomePage clickLogin()
	{
		driver.findElement(By.id(prop.getProperty("LoginPage.Button.Id"))).click();
		System.out.println("wEclocm");
		return new HomePage(driver);
	}
	
	
}
