package week5.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import week5.pom.BaseClass;

public class ContactPage extends BaseClass{
	
	public ContactPage(RemoteWebDriver driver)
	{
		this.driver=driver;
	}
	
	public ContactPage selectbutton() throws InterruptedException
	{
		Thread.sleep(10000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon")));
		By button = By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(button)));
		driver.findElement(button).click();
		return this;
	}
	
	

}
