package week5.pom.testcases;

import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class LoginTestusingPOM extends BaseClass {
	
	@Test
	public void LoginTest()
	{
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.browserClose();
	}
	

}
