package week5.pom.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class CreateDashboard extends BaseClass{
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="dashboard";
		
	}
	
	@Test (dataProvider="FetchData")
	public void CreateDashboard(String input, String dashboardText, String dummy) throws InterruptedException 
	{
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickTogglebutton()
		.clickViewAll().search(input)
		.clickDashboardSelection()
		.clickNewDashboard()
		.frameSwitch()
		.DashBoardinput(dashboardText)
		.frameback()
		.frameSwitch()
		.savechanges()
		.frameback()
		.verifyText()
		.browserClose();
	}

}
