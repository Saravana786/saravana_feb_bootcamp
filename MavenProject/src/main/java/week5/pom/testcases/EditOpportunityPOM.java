package week5.pom.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class EditOpportunityPOM extends BaseClass{
	@BeforeTest
	public void setsheet()
	{
		 sheetName="opportunity";
		
	}

	@Test (dataProvider="FetchData")
     public void EditOpportunityPOM(String text1, String text2, String text3) throws InterruptedException
	{
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.clickLogin().clickTogglebutton()
		.clickViewAll()
		.clickSalesMenu()
		.selectOpportunity()
		.searchlist()
		.refreshbutton()
		.selectOpportunityowner()
		.showactions()
		.selectEdit()
		.inputdate()
		.selectNewOpportunity()
		.selectstage()
		.selecteditstage()
		.clickvalue()
		.inputvalue()
		.editinputvalue()
		.SaveOpportunity()
		.toastvalidation()
		.browserClose();
	
	}
	
}
