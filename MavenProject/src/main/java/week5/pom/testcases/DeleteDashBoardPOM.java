package week5.pom.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class DeleteDashBoardPOM extends BaseClass{
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="dashboard";
		
	}
	
	@Test (dataProvider="FetchData")
	public void DeleteDashBoardPOM(String input, String description, String shorttext) throws InterruptedException
	
	{
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.clickLogin().clickTogglebutton()
		.clickViewAll().search(input)
		.clickDashboardSelection()
		.selectEditserach(shorttext)
		.clickDropdownDashboard()
		.deletePage()
		.clickDeletePage()
		.verifyDeleteToast()
		.browserClose();
	}
	

}
