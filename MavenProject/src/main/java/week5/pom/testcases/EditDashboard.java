package week5.pom.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class EditDashboard extends BaseClass {

	@BeforeTest
	public void setsheet()
	{
		 sheetName="dashboard";
		
	}
	
	@Test (dataProvider="FetchData")
	public void EditDashboard(String input, String description, String shorttext) throws InterruptedException
	{
		
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickTogglebutton()
		.clickViewAll().search(input)
		.clickDashboardSelection()
		.selectEditserach(shorttext)
		.clickDropdownDashboard()
		.editPage()
		.frameSwitch()
		.clickEditPage()
		.sendInput()
		.submitButton()
		.frameback()
		.frameSwitch()
		.clickDoneButton()
		.clickSavebutton()
		.verifyEditPage()
		.frameback()
		.browserClose();
		
	}
}
