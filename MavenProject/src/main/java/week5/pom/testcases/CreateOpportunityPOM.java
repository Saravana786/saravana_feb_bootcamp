package week5.pom.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.pom.BaseClass;
import week5.pom.pages.LoginPage;

public class CreateOpportunityPOM extends BaseClass{
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="opportunity";
		
	}
	
	@Test (dataProvider="FetchData")
	public void CreateOpportunityPOM(String input, String name, String text3) throws InterruptedException
	{
		new LoginPage(driver).enterUserName(username)
		.enterPassword(password)
		.clickLogin().clickTogglebutton()
		.clickViewAll()
		.clickSalesMenu()
		.selectOpportunity()
		.newOpportunity()
		.inputname(name)
		.inputCloseDate(todayDate)
		.selectNewOpportunity()
		.needAnalysisselct()
		.SaveOpportunity()
		.browserClose();
	}

}
