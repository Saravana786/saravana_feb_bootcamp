package week5.pom.commonutils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface variable {
	
	public String url="https://login.salesforce.com/";
	
	
	public static final String dashboardText = "";
	public String dashboardText1="Automation by Saravana";
	public static final String input = "";
	public String description="SalesForce";
	public String expected="Salesforce Automation by Saravana";
	public String stage="Perception Analysis";
	public String salesforce="SalesForce";
	public static final String name = "";
	public static final String lastname = "";
	public String email="saravana@test.com";
	public String credit="Saravanacredit";
	public String contact="Contacts";
	public String contactcount=null;
	public String username="makaia@testleaf.com";
	public String password="BootcampSel$123";
	public String title="Test";
	public String phone="11111111111";
	public String sales="Sales";
	public String task="Content";
	//public String filename="./testdata.xlsx";
	public String fileName= "E:\\input\\testdata.xlsx";
	 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/dd/yyy");  
	   LocalDateTime now = LocalDateTime.now();  
	   String todayDate=dtf.format(now);
	   LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);
	   String tommorowDate=dtf.format(tomorrow);

}
