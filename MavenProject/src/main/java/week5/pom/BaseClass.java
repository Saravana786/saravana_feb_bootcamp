package week5.pom;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import io.github.bonigarcia.wdm.WebDriverManager;
import week3.testcases.ReadExcel;
import week5.pom.commonutils.variable;

public class BaseClass implements variable {
	
	//public ChromeDriver driver;
	public WebDriverWait wait;
	public RemoteWebDriver driver;
	JavascriptExecutor je=(JavascriptExecutor)driver;
	public static Properties prop;
	
	public String sheetName;
	
	@BeforeSuite
	public void propertyLoad() throws IOException
	{
		FileInputStream fis = new FileInputStream("./src/main/java/week5/pom/commonutils/LocatorsProperty.properties");
		prop = new Properties();
		prop.load(fis);
	}
	
	@BeforeMethod
	public void appLaunch() 
	{
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		options.addArguments("--disable-notifications");
	       
		//Login to salesforce application
		driver.get(url);
		driver.manage().window().maximize();
		
		//wait
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		 wait = new WebDriverWait(driver,30);

	}
	
	
	@AfterMethod
	public void browserClose()
	{
		driver.quit();
	}
	

	@DataProvider(name="FetchData")
	public String[][] sendData() throws IOException {
		return ReadExcel.readExcel(fileName, sheetName);
	}
}
