package week1.testcases;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
//import io.reactivex.rxjava3.functions.Action;

public class editOpportunity {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		String expected="Salesforce Automation by Saravana";
		String stage="Perception Analysis";
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/dd/yyy");  
		  // LocalDateTime now = LocalDateTime.now();  
		   //System.out.println(dtf.format(now));  
		   LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);
		   
		
		//String todayDate="2/27/2022";
		//String todayDate=dtf.format(now);
		String tommorowDate=dtf.format(tomorrow);
		String input="SalesForce";
		
WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		//Login to salesforce application
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,30);
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		//searching for sales application
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[text()='Sales']"))));
		driver.findElement(By.xpath("//p[text()='Sales']")).click();
		
		
		WebElement element=driver.findElement(By.xpath("//*[text()='Opportunities']"));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].click();", element);
		//driver.findElement(By.linkText("Opportunities")).click();
		
		//Searching the text and click for refresh
		Thread.sleep(6000);
		driver.findElement(By.xpath("//label[text()='Search this list...']/following::input")).sendKeys(expected);
		driver.findElement(By.xpath("//button[@name='refreshButton']")).click();
		Thread.sleep(3000);
		
		WebElement element1=driver.findElement(By.xpath("//span[contains(@title,'Opportunity Owner')]"));
		JavascriptExecutor je1=(JavascriptExecutor)driver;
		je1.executeScript("arguments[0].click();", element1);
		Thread.sleep(1000);
		WebElement element2=driver.findElement(By.xpath("(//span[contains(text(),'Show Actions')])[1]"));
		JavascriptExecutor je2=(JavascriptExecutor)driver;
		je2.executeScript("arguments[0].click();", element2);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@title='Edit']")).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
		Thread.sleep(2000); 
		driver.findElement(By.xpath("//input[@name='CloseDate']")).clear();
		driver.findElement(By.xpath("//input[@name='CloseDate']")).sendKeys(tommorowDate);
		
		//WebElement element1=driver.findElement(B3y.xpath("//span[@title='Needs Analysis']"));
				WebElement element3=driver.findElement(By.xpath("(//button[contains(@class,'slds-combobox__input slds-input_faux')]//span)[2]"));
				//button[@data-position-id='lgcp-1000007']//span[1]
				JavascriptExecutor je3=(JavascriptExecutor)driver;
				je3.executeScript("arguments[0].click();", element3);
				//driver.findElement(By.xpath("//span[@title='Needs Analysis']")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//span[@title='"+stage+"']")).click();
				Thread.sleep(1000);
				WebElement element4=driver.findElement(By.xpath("(//button[contains(@class,'slds-combobox__input slds-input_faux')])[4]"));
				JavascriptExecutor je4=(JavascriptExecutor)driver;
				je4.executeScript("arguments[0].scrollIntoView(true);",element4);
				driver.findElement(By.xpath("(//button[contains(@class,'slds-combobox__input slds-input_faux')])[4]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//span[@title=\"In progress\"]")).click();
				driver.findElement(By.xpath("//textarea[@class='slds-textarea']")).clear();
				driver.findElement(By.xpath("//textarea[@class='slds-textarea']")).sendKeys(input);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
				
				WebElement element5=driver.findElement(By.xpath("//span[text()='"+stage+"']"));
				
				//System.out.println(element5.getText());
				
				if (stage.equals(element5.getText()))
				{
					System.out.println("Pass");
				}
				else
				{
					System.out.println("Fail");
				}
				
				driver.close();
						
		//driver.findElement(By.xpath("//a[contains(@title,'more actions')]")).click();

	}

}
