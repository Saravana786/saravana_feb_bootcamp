package week1.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import week3.testcases.BaseClass;

public class DashboardClasses extends BaseClass{
	
	
	
	@Test (priority=1)
	public void createNewDash() throws InterruptedException
	{
    System.out.println("HII");
	
	Thread.sleep(2000);
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
	//searchwindow.sendKeys(input);
	driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
	
	Thread.sleep(5000);
	driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	driver.findElement(By.id("dashboardNameInput")).sendKeys(dashboardText);
	Thread.sleep(1000);
	driver.findElement(By.id("submitBtn")).click();
	driver.switchTo().defaultContent();
    Thread.sleep(4000); 
    driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	Thread.sleep(4000); 

	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Save']"))));
	driver.findElement(By.xpath("//button[text()='Save']")).click();
	driver.switchTo().defaultContent();
	Thread.sleep(5000);
	String result=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
	
	if (result.contains("saved") && result.contains("Dashboard"))
	{
		System.out.println("Pass");
	}
	else
	{
		System.out.println("Fail");
	}
	
	}
	
	@Test(priority=2)
	
	public void EditDash() throws InterruptedException
	{
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
		//searchwindow.sendKeys(input);
		driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[contains(@placeholder,'Search recent dashboards')]")).sendKeys(dashboardText1);
		
		Thread.sleep(5000);
		
		WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
		
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].scrollIntoView(true);",element);
		driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Edit']")).click();
		
		Thread.sleep(4000); 
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//iframe[@title='dashboard']"))));
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
        
        Thread.sleep(10000);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']"))));
        driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']")).click();
        
        Thread.sleep(5000);
		Thread.sleep(1000);
        driver.findElement(By.id("dashboardDescriptionInput")).sendKeys(description);
        
        Thread.sleep(1000);
        
        Thread.sleep(1000);
		driver.findElement(By.id("submitBtn")).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
		//driver.findElement(By.xpath("//span[text()='Show actions']//parent::lightning-primitive-icon")).click();
		
    	 driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
 		Thread.sleep(4000); 

     	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Done']"))));
     	driver.findElement(By.xpath("//button[text()='Done']")).click();
     	
     	Thread.sleep(1000);
     	
     	driver.findElement(By.xpath("(//button[text()='Save'])[2]")).click();
     	
     	Thread.sleep(2000);
     	String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
		
     	Thread.sleep(2000);
		//String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		if (result.contains(description) )
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
		
		driver.switchTo().defaultContent();
	}
	
	@Test(priority=3)
	public void deleteDashboard() throws InterruptedException
	{
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
	//searchwindow.sendKeys(input);
	driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//input[contains(@placeholder,'Search recent dashboards')]")).sendKeys(dashboardText1);
	
	Thread.sleep(5000);
	
	WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
	
	JavascriptExecutor je=(JavascriptExecutor)driver;
	je.executeScript("arguments[0].scrollIntoView(true);",element);
	driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
	
	Thread.sleep(2000);
	
	Thread.sleep(2000);
	driver.findElement(By.xpath("//span[text()='Delete']")).click();
	
	Thread.sleep(2000);
	//(//span[text()='Delete'])[2]
			
	driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();

	Thread.sleep(2000);
 	//String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
	String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
 	
 	if (result.contains("Dashboard") && result.contains("deleted"))
	{
		System.out.println("Pass");
	}
	else
	{
		System.out.println("Fail");
	}
}

	
}
