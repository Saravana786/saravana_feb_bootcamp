package week2.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Assesment_01_Saravana {

	public static void main(String[] args) throws InterruptedException {
		
	
	//Driver initialization
		    WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			ChromeDriver driver = new ChromeDriver(options);
			
	//Login to salesforce application
			driver.get("https://login.salesforce.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			
			WebDriverWait wait = new WebDriverWait(driver,30);
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			
			
			
			
     //Inputs section
			String dashboardText="Saravanaa_Workout4";
			String description="Testing";
			String input="Service console";
			
			
			
	//searching for Service Console application
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
			driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
			driver.findElement(By.xpath("//button[text()='View All']")).click();
			
			WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
			searchwindow.sendKeys(input);
			
			driver.findElement(By.xpath("//mark[text()='Service Console']")).click();
			Thread.sleep(6000);
			
	//Selecting Dashboards from Service console screen
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title='Show Navigation Menu']//lightning-primitive-icon[1]"))));
			driver.findElement(By.xpath("//button[@title='Show Navigation Menu']//lightning-primitive-icon[1]")).click();
			driver.findElement(By.xpath("//span[contains(@class,'menuLabel') and (text()='Dashboards')]")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
			Thread.sleep(8000);
			
	//Handling frame and inputting value to New Dashboard creation
			//wait.until(ExpectedConditions.presenceOfElementLocated(driver.findElement(By.xpath("//iframe[@title='dashboard']"))));
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
			Thread.sleep(5000);
			driver.findElement(By.id("dashboardNameInput")).sendKeys(dashboardText);
			Thread.sleep(2000);
		    driver.findElement(By.id("dashboardDescriptionInput")).sendKeys(description);
			driver.findElement(By.id("submitBtn")).click();
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			
	//Save the changes by pressing Done button
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	 		Thread.sleep(6000); 
	 		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Done']"))));
	     	driver.findElement(By.xpath("//button[text()='Done']")).click();
	     	Thread.sleep(1000);
	     	
	  //Validating the creation status
	     	String result=driver.findElement(By.xpath("(//span[text()='Dashboard']//following::span)[1]")).getText();
	     	if (result.contains(dashboardText))
			{
				System.out.println("New Dashboard created successfully");
			
				//Subscribing new 
				driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
				driver.switchTo().defaultContent();
				Thread.sleep(5000);
				
				//Selecting Subscription schedule
				driver.findElement(By.xpath("//span[text()='Daily']")).click();
				Select se=new Select(driver.findElement(By.id("time")));
				se.selectByIndex(10);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//span[text()='Save']")).click();
				Thread.sleep(2000);
				String result1=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		     
			//Validating Subscription status	
				if (result1.contains("ubscription") && result1.contains("tarted"))
				{
					System.out.println("Subscription is Success");
					Thread.sleep(4000);
					driver.findElement(By.xpath("//button[@title='Close "+dashboardText+"']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[text()='Dashboards'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//a[text()='Private Dashboards'])[1]")).click();
					
					Thread.sleep(4000);
					driver.findElement(By.xpath("//input[contains(@placeholder,'Search private dashboards')]")).sendKeys(dashboardText);
								
					Thread.sleep(5000);
					//Deleting the entry
					WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
					
					JavascriptExecutor je=(JavascriptExecutor)driver;
					je.executeScript("arguments[0].scrollIntoView(true);",element);
					driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
					
					Thread.sleep(2000);
					
					Thread.sleep(2000);
					driver.findElement(By.xpath("//span[text()='Delete']")).click();
					
					Thread.sleep(2000);
							
					driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();
					
					
					Thread.sleep(2000);
					String result3=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
			     	
			     	if (result3.contains("Dashboard") && result3.contains("deleted"))
					{
						System.out.println("Test case Pass");
					}
					else
					{
						System.out.println("Test case Fail");
					}
					
					driver.quit();
					
				}
				else
				{
					System.out.println("Subscription Failed");
				}
				
			}
			else
			{
				System.out.println("Creation Failed");
			}
			
	}

}
