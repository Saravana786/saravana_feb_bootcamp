package week3.testcases;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class EditContact extends BaseClass{
	
	@Test
	public void editContact() throws InterruptedException
	{
		
		int i=0;
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
		searchwindow.sendKeys(contact);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//mark[text()='Contacts']")).click();
		Thread.sleep(2000);
		contactcount=driver.findElement(By.xpath("//span[@class='countSortedByFilteredBy']")).getText();
		System.out.println(contactcount);

		Thread.sleep(5000);
		//List<WebElement> countcheck =  driver.findElements(By.xpath("//span[contains(text(),'Select item')]"));
		List<WebElement> countcheck =  driver.findElements(By.xpath("//span[@class='slds-checkbox--faux']"));
		//System.out.println(countcheck.size());
		int countvalue=countcheck.size()-1;
		//System.out.println(countvalue);
		JavascriptExecutor je=(JavascriptExecutor)driver;
		
		while (i <=countvalue)
		{
			//System.out.println(countvalue);
			 //countvalue=countcheck.size()-1;
			if (countvalue==i )
			  {
				
				int j=i+1;
				//System.out.println("Inside loop");
				//System.out.println(countcheck.size()-1);
			
			//ac.moveToElement(element.get(i)).click().build().perform();
			//je.executeScript("arguments[0].scrollIntoView(true);",element.get());
			
		 // WebElement element1=driver.findElements(By.xpath("(//span[@class='slds-checkbox--faux'])[17]"));
			//System.out.println(countcheck);
			Thread.sleep(5000);
			je.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])["+i+"]")));
			driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])["+j+"]")).click();
			Thread.sleep(5000);
			je.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])[1]")));
			driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])[1]")).click();
			Thread.sleep(5000);
			je.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])["+i+"]")));
			driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])["+j+"]")).click();
			countcheck =  driver.findElements(By.xpath("//span[@class='slds-checkbox--faux']"));
			countvalue=countcheck.size()-1;
			
			  }
			++i;
		}
		
		//Thread.sleep(10);
		//countvalue=countcheck.size();
		System.out.println("Number of records in the list are: " +countvalue);
		Thread.sleep(5000);
		je.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])[2]")));
		driver.findElement(By.xpath("(//span[@class='slds-checkbox--faux'])[2]")).click();
		
		driver.findElement(By.xpath("//input[contains(@placeholder,'Search this list')]")).sendKeys(name+" "+lastname);
		
		driver.findElement(By.xpath("//button[@name='refreshButton']")).click();
		Thread.sleep(3000);
		WebElement element2=driver.findElement(By.xpath("(//span[contains(text(),'Show Actions')])[1]"));
		
		je.executeScript("arguments[0].click();", element2);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@title='Edit']")).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element1);
		Thread.sleep(2000); 
		
		driver.findElement(By.xpath("//input[@name='Title']")).sendKeys(title);
		
		driver.findElement(By.xpath("//input[@name='Birthdate']")).sendKeys("3/2/2022");
		Thread.sleep(5000);
		je.executeScript("arguments[0].click();", driver.findElement(By.xpath("//button[contains(@aria-label,'Lead Source')]")));
		//driver.findElement(By.xpath("//button[contains(@aria-label,'Lead Source')]")).click();
		//Thread.sleep(5000);
		driver.findElement(By.xpath("//span[@title='Purchased List']")).click();
		driver.findElement(By.xpath("//input[@name='Phone']")).sendKeys(phone);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
		String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		if (result.contains("saved") && result.contains(name))
		{
			System.out.println("Edit Contact Pass");
			Thread.sleep(2000);
			String result2=driver.findElement(By.xpath("//span[contains(@class,'OutputPhone')]")).getText();
		System.out.println(result2);
		}
		else
		{
			System.out.println("Edit Contact Fail");
		}
		
	}

}
