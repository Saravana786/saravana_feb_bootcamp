package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class createContact extends BaseClass{
	
	
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="contact";
		
	}

	@Test (dataProvider="FetchData")
	public void createcontact(String name,String lastname) throws InterruptedException
	{
		
		Thread.sleep(10000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon")));
		
		By button = By.xpath("//lightning-icon[contains(@class,'slds-icon-utility-add slds-button slds-button')]/lightning-primitive-icon");
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(button)));
		driver.findElement(button).click();
		//Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[text()='New Contact']"))));
		
		driver.findElement(By.xpath("//span[text()='New Contact']")).click();
		
		driver.findElement(By.xpath("//a[@class='select']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[@title='Mr.']"))));
		driver.findElement(By.xpath("//a[@title='Mr.']")).click();
		driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys(name);
		driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys(lastname);
		driver.findElement(By.xpath("//input[@inputmode='email']")).sendKeys(email);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@title='Search Accounts']")).click();
		Thread.sleep(2000);
		System.out.println("HI");
		
		driver.findElement(By.xpath("//span[@title='New Account']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//*[@class=' input'])[4]")).sendKeys(credit);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[text()='Save'])[3]")));
		driver.findElement(By.xpath("(//span[text()='Save'])[3]")).click();
		Thread.sleep(1000);
		
    String result=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
	    
		if (result.contains("created") && result.contains(credit))
		{
			System.out.println("Create Credit Pass");
			
			driver.findElement(By.xpath("(//span[text()='Save'])[2]")).click();
			Thread.sleep(5000);
			
			String result1=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
		    
			if (result1.contains("saved") && result1.contains(name))
			{
				System.out.println("Create Contact Pass");
			}
			else
			{
				System.out.println("Create Contact Fail");
			}
			
			
		}
		else
		{
			System.out.println("Create Credit Fail");
		}
		
		
		
		}
	
	}


