package week3.testcases;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateDashboard extends BaseClass {
	
	
	//@Test (groups = {"dashboard"})
	//@Test (retryAnalyzer = RetryClass.class)
@BeforeTest
public void setsheet()
{
	 sheetName="dashboard";
	

}
	
	@Test (dataProvider="FetchData")
	public void createNewDash(String input, String test, String dummy) throws InterruptedException, InvalidFormatException, IOException
	{
    System.out.println("HII");
   // readExcel(filename,"Sheet1",1,0);
    System.out.println(test);
	
	Thread.sleep(2000);
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
	searchwindow.sendKeys(input);
	driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
	
	Thread.sleep(5000);
	driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	driver.findElement(By.id("dashboardNameInput")).sendKeys(dashboardText);
	Thread.sleep(1000);
	driver.findElement(By.id("submitBtn")).click();
	driver.switchTo().defaultContent();
    Thread.sleep(4000); 
    driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	Thread.sleep(4000); 

	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Save']"))));
	driver.findElement(By.xpath("//button[text()='Save']")).click();
	driver.switchTo().defaultContent();
	Thread.sleep(5000);
	String result=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
	
	if (result.contains("saved") && result.contains("Dashboard"))
	{
		System.out.println("Create Dashboard Pass");
	}
	else
	{
		System.out.println("Create Dashboard Fail");
	}
	
	}

}
