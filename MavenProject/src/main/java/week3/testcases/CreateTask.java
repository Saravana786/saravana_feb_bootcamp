package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateTask extends BaseClass {

	@BeforeTest
	public void setsheet()
	{
		 sheetName="task";
		
	}
	@Test
	public void createtask() throws InterruptedException
	{
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
	searchwindow.sendKeys(task);
	driver.findElement(By.xpath("//mark[text()='Content']")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("(//span[@class='viewAllLabel' and text()='View All'])[2]")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("//span[text()='Show more actions']")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("//a[@title='New Task']")).click();
	}
}
