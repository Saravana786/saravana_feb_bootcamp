package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateAccount extends BaseClass{
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="account";
		
	}
	@Test (dataProvider="FetchData")
	public void createaccount() throws InterruptedException
	{
		int i=0;
		JavascriptExecutor je=(JavascriptExecutor)driver;
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
		searchwindow.sendKeys(sales);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//mark[text()='Sales'])[3]")).click();
		Thread.sleep(18000);
		
		//je.executeScript("return document.readyState").toString().equals("complete");
		driver.findElement(By.xpath("(//span[text()='Accounts'])[1]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//div[@title='New']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(name);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(@aria-label,'Ownership')]")).click();
		Thread.sleep(2000);
		WebElement element=driver.findElement(By.xpath("//span[@title='Public']"));
		je.executeScript("arguments[0].click();", element);
		
		
	}

}
