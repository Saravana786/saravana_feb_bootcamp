package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EditDashboard extends BaseClass {
	
	@BeforeTest
	public void setsheet()
	{
		 sheetName="dashboard";
		
	}
	
@Test(groups = {"dashboard"},dataProvider="FetchData")
	
	public void EditDash(String test, String description) throws InterruptedException
	{
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
		searchwindow.sendKeys(input);
		driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[contains(@placeholder,'Search recent dashboards')]")).sendKeys(dashboardText1);
		
		Thread.sleep(5000);
		
		WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
		
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].scrollIntoView(true);",element);
		driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Edit']")).click();
		
		Thread.sleep(4000); 
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//iframe[@title='dashboard']"))));
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
        
        Thread.sleep(10000);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']"))));
        driver.findElement(By.xpath("//button[@title='Edit Dashboard Properties']")).click();
        
        Thread.sleep(5000);
		Thread.sleep(1000);
        driver.findElement(By.id("dashboardDescriptionInput")).sendKeys(description);
        
        Thread.sleep(1000);
        
        Thread.sleep(1000);
		driver.findElement(By.id("submitBtn")).click();
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
		//driver.findElement(By.xpath("//span[text()='Show actions']//parent::lightning-primitive-icon")).click();
		
    	 driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
 		Thread.sleep(4000); 

     	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Done']"))));
     	driver.findElement(By.xpath("//button[text()='Done']")).click();
     	
     	Thread.sleep(1000);
     	
     	driver.findElement(By.xpath("(//button[text()='Save'])[2]")).click();
     	
     	Thread.sleep(2000);
     	String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
		
     	Thread.sleep(2000);
		//String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		if (result.contains(description) )
		{
			System.out.println("Edit Dashboard Pass");
		}
		else
		{
			System.out.println("Edit Dashboard Fail");
		}
		
		driver.switchTo().defaultContent();
	}
	

}
