package week3.testcases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseClass  {
	public RemoteWebDriver driver;
	public WebDriverWait wait;
	public String dashboardText;
	public String dashboardText1="Automation by Saravana";
	public String input;
	public String description="SalesForce";
	public String expected="Salesforce Automation by Saravana";
	public String stage="Perception Analysis";
	public String salesforce="SalesForce";
	public String name;
	public String lastname;
	public String email="saravana@test.com";
	public String credit="Saravanacredit";
	public String contact="Contacts";
	public String contactcount=null;
	public String username="makaia@testleaf.com";
	public String password="BootcampSel$123";
	public String title="Test";
	public String phone="11111111111";
	public String sales="Sales";
	public String task="Content";
	//public String filename="./testdata.xlsx";
	public String fileName= "E:\\input\\testdata.xlsx";
	
	public String sheetName;
	


	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/dd/yyy");  
	   LocalDateTime now = LocalDateTime.now();  
	   String todayDate=dtf.format(now);
	   LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);
	   String tommorowDate=dtf.format(tomorrow);
	   
	
	@SuppressWarnings("deprecation")
	
	@BeforeMethod (groups = {"dashboard", "Opportunity"})
	//@Parameters({"username","password"})
	// public void driverstart(String username, String password){
		 public void driverstart(){

		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		options.addArguments("--disable-notifications");
	    wait = new WebDriverWait(driver,30);
		
		//Login to salesforce application
	driver.get("https://login.salesforce.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	
	driver.findElement(By.id("username")).sendKeys(username);
	driver.findElement(By.id("password")).sendKeys(password);
	driver.findElement(By.id("Login")).click();
	
	}
	
	@AfterMethod (groups = {"dashboard", "Opportunity"})
	public void closingBrowser()
	{
		System.out.println("Inside After Method");

		driver.quit();
	}
	
	@DataProvider(name="FetchData")
	public String[][] sendData() throws IOException {
		return ReadExcel.readExcel(fileName, sheetName);
	}

}
