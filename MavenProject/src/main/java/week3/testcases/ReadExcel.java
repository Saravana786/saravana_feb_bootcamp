package week3.testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel extends BaseClass{
	

		public static String[][] readExcel(String Filename, String sheetName) throws IOException {
			
			XSSFWorkbook wb = new XSSFWorkbook(Filename);
			XSSFSheet ws = wb.getSheet(sheetName);
			int row = ws.getLastRowNum();
			int col = ws.getRow(0).getLastCellNum();
			String[][] cellvalue = new String[row][col];
			
			for(int i=1; i<=row; i++)
			{
				
				for(int j=0; j<col; j++)
				{
					String cell = ws.getRow(i).getCell(j).getStringCellValue();
					cellvalue[i-1][j] = cell;
					System.out.println(cell);
				}
			}
			wb.close();
			return cellvalue;
		}

	}



