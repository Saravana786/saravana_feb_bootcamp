package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class DeleteDashboard extends BaseClass {
	
	@Test(dataProvider="FetchData")
	public void deleteDashboard(String input, String description, String dashboardText1) throws InterruptedException
	{
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
	searchwindow.sendKeys(input);
	driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
	Thread.sleep(1000);
	driver.findElement(By.xpath("//input[contains(@placeholder,'Search recent dashboards')]")).sendKeys(dashboardText1);
	
	Thread.sleep(5000);
	
	WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
	
	JavascriptExecutor je=(JavascriptExecutor)driver;
	je.executeScript("arguments[0].scrollIntoView(true);",element);
	driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
	
	Thread.sleep(2000);
	
	Thread.sleep(2000);
	driver.findElement(By.xpath("//span[text()='Delete']")).click();
	
	Thread.sleep(2000);
	//(//span[text()='Delete'])[2]
			
	driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();

	Thread.sleep(2000);
 	//String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
	String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
 	
 	if (result.contains("Dashboard") && result.contains("deleted"))
	{
		System.out.println("Delete Dashboard Pass");
	}
	else
	{
		System.out.println("Delete Dashboard Fail");
	}
}

}
