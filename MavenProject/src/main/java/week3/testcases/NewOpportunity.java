package week3.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class NewOpportunity extends BaseClass{
	@Test (groups = {"Opportunity"})
	public void createOpporutunity() throws InterruptedException {
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
	driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
	driver.findElement(By.xpath("//button[text()='View All']")).click();
	wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//p[text()='Sales']"))));
	driver.findElement(By.xpath("//p[text()='Sales']")).click();
	
	//Searching for Opportunity tab
	WebElement element=driver.findElement(By.xpath("//*[text()='Opportunities']"));
	wait.until(ExpectedConditions.elementToBeClickable(element));
	
	JavascriptExecutor je=(JavascriptExecutor)driver;
	je.executeScript("arguments[0].click();", element);
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@title='New']"))));
	driver.findElement(By.xpath("//div[@title='New']")).click();
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='Name']"))));
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@name='CloseDate']"))));
	driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(expected);
	driver.findElement(By.xpath("//input[@name='CloseDate']")).sendKeys(todayDate);
	WebElement element1=driver.findElement(By.xpath("//button[contains(@class,'slds-combobox__input slds-input_faux')]//span"));
	wait.until(ExpectedConditions.elementToBeClickable(element1));
	JavascriptExecutor je1=(JavascriptExecutor)driver;
	je1.executeScript("arguments[0].click();", element1);
	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[@title='Needs Analysis']"))));
	driver.findElement(By.xpath("//span[@title='Needs Analysis']")).click();
	driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
	
	//Validating results
	Thread.sleep(2000);
	String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
	if (result.contains("created") && result.contains(expected))
	{
		System.out.println("Create Opporunity Pass");
	}
	else
	{
		System.out.println("Create Opporunity Fail");
	}
	}
}
